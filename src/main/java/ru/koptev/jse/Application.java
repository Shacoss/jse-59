package ru.koptev.jse;

import ru.koptev.jse.model.Bus;
import ru.koptev.jse.model.ElectricCar;
import ru.koptev.jse.model.SportsCar;
import ru.koptev.jse.model.Truck;
import ru.koptev.jse.repository.CommonRepository;
import ru.koptev.jse.repository.impl.CommonRepositoryImpl;

import java.time.Instant;

public class Application {

    public static void main(String[] args) {
        CommonRepository commonRepository = new CommonRepositoryImpl();
        SportsCar sportsCar = SportsCar.builder()
                .brand("Audi")
                .model("R8")
                .year(Instant.now())
                .horsePower(420)
                .acceleration(4)
                .build();
        ElectricCar electricCar = ElectricCar.builder()
                .brand("Tesla")
                .model("Model X")
                .year(Instant.now())
                .maxDistance(550)
                .build();
        Bus bus = Bus.builder()
                .brand("Volkswagen")
                .model("Transporter")
                .year(Instant.now())
                .maxPassengers(9)
                .build();
        Truck truck = Truck.builder()
                .brand("Mercedes-Benz")
                .model("Actros")
                .year(Instant.now())
                .carrying(15)
                .build();
        commonRepository.save(sportsCar);
        commonRepository.save(electricCar);
        commonRepository.save(bus);
        commonRepository.save(truck);
    }

}
