package ru.koptev.jse.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@Table(name = "electric_car")
@EqualsAndHashCode(callSuper = true)
public class ElectricCar extends Vehicle {

    @Column(name = "max_distance", length = 10)
    private Integer maxDistance;

}
