package ru.koptev.jse.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@Table(name = "truck")
@EqualsAndHashCode(callSuper = true)
public class Truck extends Vehicle {

    @Column(length = 10)
    private Integer carrying;

}
