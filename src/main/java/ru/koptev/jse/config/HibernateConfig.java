package ru.koptev.jse.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import ru.koptev.jse.model.Bus;
import ru.koptev.jse.model.ElectricCar;
import ru.koptev.jse.model.SportsCar;
import ru.koptev.jse.model.Truck;
import ru.koptev.jse.model.Vehicle;

public class HibernateConfig {

    private static SessionFactory sessionFactory = null;

    private HibernateConfig() {

    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) {
            return sessionFactory;
        }
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Bus.class);
        configuration.addAnnotatedClass(ElectricCar.class);
        configuration.addAnnotatedClass(SportsCar.class);
        configuration.addAnnotatedClass(Truck.class);
        configuration.addAnnotatedClass(Vehicle.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

}
