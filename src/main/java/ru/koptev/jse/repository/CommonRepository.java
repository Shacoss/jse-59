package ru.koptev.jse.repository;

import java.util.List;
import java.util.Optional;

public interface CommonRepository {

    <T> Optional<T> save(T entity);

    <T> List<T> getAll(Class<T> tClass);

}
