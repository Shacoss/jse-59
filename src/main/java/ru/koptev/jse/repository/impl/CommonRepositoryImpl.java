package ru.koptev.jse.repository.impl;

import lombok.extern.log4j.Log4j2;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.koptev.jse.config.HibernateConfig;
import ru.koptev.jse.repository.CommonRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Log4j2
public class CommonRepositoryImpl implements CommonRepository {

    private final SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    @Override
    public <T> Optional<T> save(T entity) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(entity);
            transaction.commit();
            return Optional.of(entity);
        } catch (HibernateException e) {
            log.error(e.getMessage());
            if (transaction != null) {
                transaction.rollback();
            }
            return Optional.empty();
        }
    }

    @Override
    public <T> List<T> getAll(Class<T> tClass) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("FROM " + tClass.getSimpleName()).getResultList();
        } catch (HibernateException e) {
            log.error(e.getMessage());
            return Collections.emptyList();
        }
    }

}
